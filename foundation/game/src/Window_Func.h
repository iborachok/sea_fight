#ifndef FIGHT_WINDOW_FUNC_H
#define FIGHT_WINDOW_FUNC_H
#include"stdafx.h"
#include"Window.h"
#include"Game.h"
namespace fight
{
	//����, ���� ������ ������� ������ ��� ������ � �����
	class Window_Func
	{
	public:
		Window_Func()
		{
		}
		~Window_Func()
		{}
		static void display_func(void);
		static void reshape_func(int, int);
		static void key_func(unsigned char, int, int);
		static void spec_key_func(int, int, int);
		static void mouse_motion_func(int, int);
		static void mouse_func(int, int, int, int);
		static void set_game(Game game);
		static void func_thread();
		static void action_with_move_1(bool, Board_Position);
		static void action_with_move_2(bool, Board_Position);
		static void action_with_move(bool, Board_Position, int);

	private:
		static int x1, y1;
		static bool _is_selected_figure;
		static Window _window;
		static Game _game;
	};

}

#endif