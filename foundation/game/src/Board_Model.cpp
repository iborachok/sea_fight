#include"stdafx.h"
#include"Board_Model.h"


namespace fight
{
	/*Move*/

	bool Board_Model::Move::is_possible_attack(const Board_Position& dest)
	{
		if(_cell.is_selected())
		{
			set_state(is_selected_dest);
			return false;
		}
		return true;
	}

	bool Board_Model::Move::move_attac(const Board_Position& dest)
	{
		if(!is_possible_attack(dest)) return false;
		
		set_state(Move::attack);
		_cell.set_selected();

		return true;
	}


	/*Board_Model*/
	
	bool Board_Model::is_valid_pos(const Board_Position& dest)
	{
		bool is_v = true;
		
		if(!(isvalid(dest) && _board_cell[dest].is_empty()))
			{
				is_v = false;
				set_state(no_valid_dest);
			}
		return is_v;
	}

	bool Board_Model::isvalid(const Board_Position& dest) const
	{
		return is_valid(dest) && ( (dest.get_row() >= f_boundary.get_row() && dest.get_row() <= s_boundary.get_row() ) && (dest.get_column() >= f_boundary.get_column() && dest.get_column() <= s_boundary.get_column()) );
	}

	bool Board_Model::set_ship(const Board_Position& f_pos, const Board_Position& l_pos)
	{
		bool isvalid_pos = true;
		
		if(!(is_valid_pos(f_pos) && is_valid_pos(l_pos)))
		{
			isvalid_pos = false;
			return isvalid_pos;
		}

		_board_cell[f_pos].set_end_fig_left();
		_board_cell[l_pos].set_end_fig_right();

		Position_Conteiner& pos_cont = get_array_of_position(f_pos, l_pos);
		//����� � ������� ���������� ��� ���������
		if(pos_cont.size() > 1)
		{
			for(pos_cont_iterator iter = pos_cont.begin() + 1; iter != pos_cont.end() - 1; iter++)
			{
				if(!is_valid_pos(*iter))
				{
					isvalid_pos = false;
					break;
				}
			}
		}
		if(isvalid_pos)
		{
			for(pos_cont_iterator iter = pos_cont.begin(); iter != pos_cont.end(); iter++)
			{
				_board_cell[*iter].set_figure();
			}
		}
		return isvalid_pos;
	}

	bool Board_Model::move_attac(const Board_Position& dest)
	{
		bool Is_Valid = false;
		if(isvalid(dest))
		{
			_move.set_cell(_board_cell[dest]);
			_board_cell[dest].set_selected();
			if(!_move.move_attac(dest))
			{
				set_state(bed_attack);
			}
			else
			{
				Is_Valid = true;

				if(is_kill(dest))
				{
					set_state(kill);
				}
				else if(isnotkill(dest))
				{
					set_state(not_kill);
				}
				else
				{
					set_state(empty);
				}
			}
		}
		return Is_Valid;
	}

	void Board_Model::set_boundary(const Board_Position& first, const Board_Position& second)
	{
		f_boundary = first;
		s_boundary = second;
	}

	bool Board_Model::is_kill(const Board_Position& dest)
	{
		bool result = true;
		
		if(!isnotkill(dest))
		{
			result = false;
			return result;
		}

		Board_Position b_pos_l, b_pos_r;
		bool is_row = true;
		do
		{
			std::pair<Board_Position&, Board_Position&>& _pair = get_boundary_of_fig_in_this_line(dest, f_boundary, s_boundary, is_row);
			b_pos_l = _pair.first;
			b_pos_r = _pair.second;
			
			if(!is_valid(b_pos_l) || !is_valid(b_pos_r))
			{
				result = false;
				break;
			}

			Position_Conteiner& conteiner = get_array_of_position(b_pos_l, b_pos_r);
			for (pos_cont_iterator iterator = conteiner.begin(); iterator != conteiner.end(); iterator++)
			{
				if(!_board_cell[*iterator].is_selected_figure())
				{
					result = false;
					break;
				}
			}
			if(result) break;
			if(is_row) is_row = false;
			else is_row = true; //��� ���� ��� �����
		}
		while(!is_row);
		return result;
	}
	
	std::pair<Board_Position&,Board_Position&> Board_Model::get_boundary_of_fig_in_this_line(const Board_Position& dest, const Board_Position& first, const Board_Position& second, bool is_row)
	{
		Board_Position b_pos_l , b_pos_r;
		Position_Conteiner conteiner, conteiner1;
		if(is_row)
		{
			conteiner = get_array_of_position(Board_Position(dest.get_row(), first.get_column()), Board_Position(dest.get_row(), dest.get_column()));
		}
		else
		{
			conteiner = get_array_of_position(Board_Position(first.get_row(), dest.get_column()), Board_Position(dest.get_row(), dest.get_column()));
		}

		for(Position_Conteiner::iterator iterator = conteiner.begin(); iterator != conteiner.end(); iterator++)
		{
			if(_board_cell[*iterator].is_end_figure_left())
			{
				b_pos_l = *iterator;
			}
		}
		
		if(is_row)
		{
			conteiner1 = get_array_of_position(Board_Position(dest.get_row(), dest.get_column()), Board_Position(dest.get_row(), second.get_column()));
		}
		else
		{
			conteiner1 = get_array_of_position(Board_Position(dest.get_row(), dest.get_column()), Board_Position(second.get_row(), dest.get_column()));
		}
		
		for(Position_Conteiner::iterator iterator = conteiner1.begin(); iterator != conteiner1.end(); iterator++)
		{
			if(_board_cell[*iterator].is_end_figure_right())
			{
				b_pos_r = *iterator;
				break;
			}
		}

		return std::make_pair(b_pos_l, b_pos_r);
	}


	bool Board_Model::isnotkill(const Board_Position& dest)
	{
		return _board_cell[dest].is_figure();
	}

	Position_Conteiner	get_array_of_position(const Board_Position& f_pos, const Board_Position& l_pos)
	{
		Position_Conteiner array;
		unsigned first_row = f_pos.get_row(), 
			first_column = f_pos.get_column(),
			second_row = l_pos.get_row(),
			second_column = l_pos.get_column();
		for(unsigned i = first_row; i <= second_row; i++)
		{
			for(unsigned j = first_column; j <= second_column; j++)
			{
				array.push_back(Board_Position(i, j));
			}
		}
		return array;
	}

	void Board_Model::init_board(int c_size, int r_size)
	{
		for(int i = 0; i < r_size; i++)
		{
			for(int j = 0; j < c_size; j++)
			{
				_board_cell[Board_Position(i,j)].set_empty();
			}
		}
	}
}