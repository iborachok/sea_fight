#include"stdafx.h"
#include"Figure.h"
#include"Canvas2D.h"

namespace fight
{
	void Figure::draw_figure()
	{
		Canvas2D::Draw_Rectungular(_position);
	}
	void Figure::draw_figure_with_texture()
	{
		Canvas2D::Draw_Rectungular_with_Texture(_position, _texture);
	}
	void Figure::move_to(Position position[count_of_point])
	{
		for (int i = 0; i < count_of_point; i++)
		{
			_position[i] = position[i];
		}
	}
	void Figure::move_to_delta(int delta_x, int delta_y)
	{
		for (int i = 0; i < count_of_point; i++)
		{
			_position[i].set_x( _position[i].get_x() + delta_x);
			_position[i].set_y( _position[i].get_y() + delta_y);
		}
	}
	void Figure::get_position(Position out[count_of_point])
	{
		for (int i = 0; i < count_of_point; i++)
		{
			out[i] = _position[i];
		}
	}
}