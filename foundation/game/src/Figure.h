#ifndef FIGHT_SHEEP_H
#define FIGHT_SHEEP_H

#include"WindowPosition.h"
#include"Texture.h"

namespace fight
{
	class Figure
	{
	public:
		Figure(Position position[count_of_point]):_texture(Texture_Board())
		{
			move_to(position);
		}
		Figure()
		{
			_valid = false;
		}
		Figure(Position position[count_of_point], Texture_Board texture):_texture(texture)
		{
			move_to(position);
			_valid = true;
		}
		~Figure()
		{}

		void draw_figure();
		void draw_figure_with_texture();
		void move_to(Position position[count_of_point]);
		void move_to_delta(int delta_x, int delta_y);
		void get_position(Position out[count_of_point]);
		void set_texture(const Texture_Board& texture)
		{
			_texture = texture;
		}
		bool is_valid()
		{
			return _valid;
		}

		Texture_Board& get_texture()
		{
			return _texture;
		}

	private:
		Position _position[count_of_point];
		Texture_Board _texture;
		bool _valid;
	};
}

#endif