#ifndef FIGHT_TEXTURE_H
#define FIGHT_TEXTURE_H


namespace fight
{
#define texture_width 64
#define texture_height 64
#define count_of_line_in_texture 10
#define width_of_quad 0x04
#define width_of_quad_dec 4
	struct color
	{
		int red,
			blue,
			green;
		color(int r = 0, int g = 0, int b = 0):red(r), green(g), blue(b)
		{}
	};

	class Texture_Board
	{
	public:
		Texture_Board():_width(texture_width), _height(texture_height)
		{}
		~Texture_Board();

		void Create_Texture(color first_color, color second_color);
		void Set_Color(int x_pos, int y_pos, color c);

		int get_width() const
		{
			return _width;
		}

		int get_height() const
		{
			return _height;
		}

		void get_image(unsigned char image[texture_height][texture_width][3]) const;
	private:
		int _width, _height;
		unsigned char _image[texture_height][texture_width][3];
	};
}
#endif