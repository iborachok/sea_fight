#include"stdafx.h"
#include"Game.h"

namespace fight
{
	void Game::Start_Game()
	{
		Board_Position b_pos;
		do
		{
			Move(_first_player, _second_player, 1);
			Sleep(1000);
			Move(_second_player, _first_player, 2);
			
			//������� ������� �������
			_first_player->reset_pos();
			
			if(_first_player->get_play().get_count_of_ship() == 0 || _second_player->get_play().get_count_of_ship() == 0) _state = Game_Finished;
		} while (_state != Game_Finished);
	}

	void Game::InitPlayer(Player* player)
	{
		Position_Conteiner conteiner;
		do
		{
			player->form_pos_to_set_ship(conteiner);
		} 
		while(!player->get_play().set_ship(conteiner));

	}
	void Game::InitGame()
	{
		InitPlayer(_first_player);
		InitPlayer(_second_player);
	}

	void Game::Move(Player* who, Player* where, int id)
	{
		who->set_state(false);
		do
		{
			Board_Position& b_pos = who->get_pos_of_move();
			where->move_attack(b_pos);
			b_pos = Board_Position();
			who->set_state(true);
			std::cout<<id<<":\t"<<(where->get_play().get_result_of_command())<<std::endl;
		}
		while(!where->get_play().is_the_end_of_move());
		who->set_state(false);
	}
}