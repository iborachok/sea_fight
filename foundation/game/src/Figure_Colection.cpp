#include"stdafx.h"
#include"Figure_Colection.h"
namespace fight
{
	Figure& Figure_Colection::in_figure(Position pos)
	{
		bool result = false;
		int i = -1;
		for (figure_iterator iterator = get_colection().begin(); iterator != get_colection().end() - count_of_state_figure; iterator++)
		{
			i++;
			Position fig_pos[count_of_point];
			iterator->get_position(fig_pos);
			if(fig_pos[0] < pos && fig_pos[2] > pos)
			{
				result = true;
				break;
			}
		}
		if(result) return _figure_conteiner[i];
		else return Figure();
	}
	bool Figure_Colection::if_in_figure(Figure& fig,Position pos)
	{
		bool result = false;
		Position fig_pos[count_of_point];
		fig.get_position(fig_pos);
		if(fig_pos[0] < pos && fig_pos[2] > pos)
		{
			result = true;
		}
		return result;
	}

	void Figure_Colection::init_figures(int _width, int _height)
	{
		_figure_conteiner.clear();

		//1 �����
		Position position_board[count_of_point];
		position_board[0].set_x(_width / 4); position_board[0].set_y(_height / 4);
		position_board[1].set_x(_width / 4); position_board[1].set_y(3 * _height / 4);
		position_board[2].set_x(_width / 2); position_board[2].set_y(3 * _height / 4);
		position_board[3].set_x(_width / 2); position_board[3].set_y(_height / 4);
		Texture_Board texture;
		color first_color, second_color;
		first_color.blue = 255;
		second_color.green = 255;
		second_color.blue = 255;

		texture.Create_Texture(first_color, second_color);
		Figure board(position_board, texture);

		//2 �����
		int delta = _width / 3;

		position_board[0].set_x(_width / 4 + delta); position_board[0].set_y(_height / 4 );
		position_board[1].set_x(_width / 4 + delta); position_board[1].set_y(3 * _height / 4);
		position_board[2].set_x(_width / 2 + delta); position_board[2].set_y(3 * _height / 4);
		position_board[3].set_x(_width / 2 + delta); position_board[3].set_y(_height / 4);

		Texture_Board texture_s;
		texture_s.Create_Texture(first_color, second_color);
		Figure second_board(position_board, texture_s);


		//������
		int width_sheep	=  std::abs(position_board[0].get_x() - position_board[2].get_x()) / count_of_line_in_texture,
			height_sheep = std::abs(position_board[0].get_y() - position_board[1].get_y()) / count_of_line_in_texture;
		int delta_sheep = width_sheep * 2;
		
		int count = 0;
		for(int i = 4; i > 0; i--)
		{
			for(int j = 0; j < 4 - i + 1; j++)
			{
				count++;
				position_board[0].set_x(delta_sheep); position_board[0].set_y(3 * _height / 4 - height_sheep * count);
				position_board[1].set_x(delta_sheep); position_board[1].set_y(3 * _height / 4 - height_sheep * (count - 1));
				position_board[2].set_x(delta_sheep + i * width_sheep); position_board[2].set_y(3 * _height / 4 - height_sheep * (count - 1));
				position_board[3].set_x(delta_sheep + i * width_sheep); position_board[3].set_y(3 * _height / 4 - height_sheep * count);
				Figure sheep(position_board);
				add_new_figure(sheep);
			}
		}

		add_new_figure(board);
		add_new_figure(second_board);
	}

	void Figure_Colection::convert_prom_pos_koordinat_to_board(Figure& board, Position pos, int& x, int& y)
	{
		Position pos_board[count_of_point];
		board.get_position(pos_board);
		if(!if_in_figure(board, pos))
		{
			x = -1;
			y = -1;
			return;
		}

		//������ � ������ ���� �������
		int width_sheep	=  std::abs(pos_board[0].get_x() - pos_board[2].get_x()) / count_of_line_in_texture,
			height_sheep = std::abs(pos_board[0].get_y() - pos_board[1].get_y()) / count_of_line_in_texture;
		x = (pos.get_x() - pos_board[0].get_x()) / width_sheep;
		y = (pos.get_y() - pos_board[0].get_y()) / height_sheep;
	}

	void Figure_Colection::get_near_pos(Figure& board, Figure& figure)
	{
		Position pos_board[count_of_point], current_pos[count_of_point];
		
		figure.get_position(current_pos);
		board.get_position(pos_board);
		
		int x, y;
		convert_prom_pos_koordinat_to_board(board, current_pos[0], x, y);

		if(x<0) x =0;
		if(y<0) y =0;
		if(x < 0 && y < 0) return;
		//������ � ������ ���� �������
		int width_sheep	=  std::abs(pos_board[0].get_x() - pos_board[2].get_x()) / count_of_line_in_texture,
			height_sheep = std::abs(pos_board[0].get_y() - pos_board[1].get_y()) / count_of_line_in_texture;


		x *= width_sheep;
		y *= height_sheep;
		figure.move_to_delta(x - current_pos[0].get_x() + pos_board[0].get_x(),-( y - current_pos[0].get_y() + pos_board[0].get_y()));

	}

	void Figure_Colection::re_init_figures()
	{
		for (figure_iterator iterator = get_colection().begin(); iterator != get_colection().end() - count_of_state_figure; iterator++)
		{
			this->get_near_pos(*(_figure_conteiner.end() - 2 ),*iterator);
		}
	}

	void Figure_Colection::get_pos_conteiner_from_sheep(Position_Conteiner& conteiner)
	{
		int x,y;
		for (figure_iterator iterator = get_colection().begin(); iterator != get_colection().end() - count_of_state_figure; iterator++)
		{
			Position pos[count_of_point];
			iterator->get_position(pos);
			this->convert_prom_pos_koordinat_to_board(*(_figure_conteiner.end() - 2 ), pos[0], x, y);
			conteiner.push_back(Board_Position(x,y));
			this->convert_prom_pos_koordinat_to_board(*(_figure_conteiner.end() - 2 ), pos[3], x, y);
			conteiner.push_back(Board_Position(x,y));
		}
	}

}