#ifndef FIGHT_FIGURE_COLECTION
#define FIGHT_FIGURE_COLECTION

#include"stdafx.h"
#include"Figure.h"
#include"Board_Model.h"


namespace fight
{
#define count_of_state_figure 2
	typedef std::vector<Figure> Figure_Conteiner;
	typedef std::vector<Figure>::iterator figure_iterator;

	class Figure_Colection
	{
	public:
		Figure_Colection()
		{}
		~Figure_Colection()
		{
			_figure_conteiner.clear();
		}
		Figure_Conteiner& get_colection()
		{
			return _figure_conteiner;
		}
		void add_new_figure(Figure figure)
		{
			_figure_conteiner.push_back(figure);
		}

		Figure& in_figure(Position pos);
		bool if_in_figure(Figure& ,Position pos);

		void init_figures(int width, int height);

		//������� �� ������ �� �����
		void re_init_figures();

		void convert_prom_pos_koordinat_to_board(Figure& board, Position pos, int& x, int& y);

		void get_pos_conteiner_from_sheep(Position_Conteiner& out);


	private:
		Figure_Conteiner _figure_conteiner;
		void get_near_pos(Figure& board, Figure&); 
	};
	
	
}

#endif