#ifndef FIGHT_EVENTWATCHER_H
#define FIGHT_EVENTWATCHER_H

#include"EventManager.h"

namespace fight
{

	//����, ���� ������ ���������� ��������� �� ����
	class EventWatcher
	{
	public:
		EventChangeHandler convert(void(*f)(Event*))
	{
		return f;
	}
		EventWatcher(EventManager event_manager):_event_manager(event_manager) 
		{
			//_event_manager.set_event_handler((EventChangeHandler)&EventWatcher::OnEventChange);
		}
		~EventWatcher()
		{}

	public:
		void OnEventChange(Event* p_event)
		{
			switch (p_event->get_Type())
			{
			case fight::Event::Event_Type_NONE:
				break;
			case fight::Event::Event_Type_CLOSE:
				break;
			case fight::Event::Event_Type_KEYBOARD: OnKeyBoardEvent(p_event);
				break;
			case fight::Event::Event_Type_MOUSE: OnMouseEvent(p_event);
				break;
			default:
				break;
			}
		}

	private:
		EventManager _event_manager;

		void OnMouseEvent(Event*);
		void OnKeyBoardEvent(Event*);

	};

}

#endif