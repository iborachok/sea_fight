#include"stdafx.h"
#include"InitGraph.h"
#include"Player.h"

namespace fight
{
	void InitGraph::graph_main(int argc, char** argv, int w, int h)
	{
		int count = count_of_line_in_texture;
		Board_Model first_bm(count, count), second_bm(count, count);
		first_bm.set_boundary(Board_Position(0,0), Board_Position(count - 1, count - 1));
		second_bm.set_boundary(Board_Position(0,0), Board_Position(count - 1, count - 1));
		Play first_play(first_bm);
		Play second_play(second_bm);
		Player* first_player = new HumanPlayer(first_play);
		Player* second_player = new Computer_Player(second_play);
		first_player->action_with_move = Window_Func::action_with_move_1;
		second_player->action_with_move = Window_Func::action_with_move_2;
		Game g(first_player, second_player);
		
		Window_Func::set_game(g);
		
		init_func();
		_canvas2D.canvas_main(argc, argv, w, h);

		delete first_player, second_player;
	}
	
	void InitGraph::init_func()
	{
		_canvas2D.set_display_func(Window_Func::display_func);
		_canvas2D.set_key_func(Window_Func::key_func);
		_canvas2D.set_mouse_func(Window_Func::mouse_func);
		_canvas2D.set_reshape_func(Window_Func::reshape_func);
		_canvas2D.set_mouse_motion_func(Window_Func::mouse_motion_func);
		_canvas2D.set_spec_key_funcvoid(Window_Func::spec_key_func);
	}
}