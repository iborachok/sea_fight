#ifndef FIGHT_CANVAS2D
#define FIGHT_CANVAS2D

#include"WindowPosition.h"
#include"Texture.h"
#include"Figure_Colection.h"

namespace fight
{
	class Canvas2D
	{
	public:
		Canvas2D()
		{}
		~Canvas2D()
		{}

		static void init();
		void canvas_main(int argc, char**argv, int width, int height);
		static void Draw_Rectungular(Position rect[count_of_point]);
		static void Draw_Rectungular_with_Texture(Position rect[count_of_point], Texture_Board);
		
		void set_display_func(void (*display)(void))
		{
			_display_func = display;
		}
		void set_reshape_func(void (*reshape)(int, int))
		{
			_reshape_func = reshape;
		}
		void set_key_func(void (*key_func)(unsigned char, int, int))
		{
			_key_func = key_func;
		}
		void set_mouse_motion_func(void (*mouse_motion_func)(int, int))
		{
			_mouse_motion_func = mouse_motion_func;
		}
		void set_mouse_func(void (*mouse_func)(int, int, int, int))
		{
			_mouse_func = mouse_func;
		}
		void set_spec_key_funcvoid (void(*spec_key_func)(int,int,int))
		{
			_spec_key_func = spec_key_func;
		}
	private:
		static void init_texture(Texture_Board);

		void (*_display_func)(void);
		void (*_reshape_func)(int, int);
		void (*_key_func)(unsigned char, int, int);
		void (*_mouse_motion_func)(int, int);
		void (*_mouse_func)(int, int, int, int);
		void (*_spec_key_func)(int,int,int);

	};
}

#endif