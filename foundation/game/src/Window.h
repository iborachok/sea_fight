#ifndef FIGHT_WINDOW_H
#define FIGHT_WINDOW_H
#include"Canvas2D.h"
#include"Figure_Colection.h"
#include"GlobalVar.h"

namespace fight
{
	//Figure_Colection _figure_colection;
	class  Window
	{
	public:
		Window(int width = 500, int height = 500):_width(width), _height(height)
		{
			_figure_colection.init_figures(_width, _height);
			_state_game = false;
		}
		~ Window()
		{}

		void set_width(int width)
		{
			_width = width;
		}
		void set_height(int height)
		{
			_height = height;
		}
		void set_state(bool state)
		{
			_state_game = state;
		}

		void draw_scen();

		Figure_Colection& get_colection()
		{
			return _figure_colection;
		}
		bool get_state() const
		{
			return _state_game;
		}
		int get_width() const
		{
			return _width;
		}
		int get_height() const
		{
			return _height;
		}

	private:
		int _width, _height;
		Figure_Colection _figure_colection;
		bool _state_game;


	};

}
#endif