#include"stdafx.h"
#include"Player.h"

namespace fight
{
#define size_of_board 10
#define count_of_deck 4

	//Computer_Player
	
	Board_Position& Computer_Player::rand_pos()
	{
		Board_Position b_pos;
			unsigned r = rand()%size_of_board,
					c =rand()%size_of_board;
			b_pos.set_column(c);
			b_pos.set_row(r);
		return b_pos;
	}

	Board_Position& Computer_Player::get_pos_of_move()
	{
		init_good_pos_and_delta();
		if(is_valid(current_pos))
		{
			current_pos.set_row(current_pos.get_row() + _delta.delta_r);
			current_pos.set_column(current_pos.get_column() + _delta.delta_c);
		}
		else
		{
			current_pos = rand_pos();
		}
		return current_pos;

	}


	void Computer_Player::init_good_pos_and_delta()
	{
		//
		do
		{
			if(get_state())
			{
				//������ ������� ������
				break;
			}
			
			//������� ������� �� �����, ������
			//����������� �� ����������� �����
			current_pos = good_pos;
			if(!is_valid(current_pos))
			{
				//current_pos = Board_Position();
				break;
			}
			//������ ������:
			//������: �� ����������� �������
			//�� ������������� �� ���
			//��� �� ����
			if(_delta.delta_c = -1)
			{
				_delta.delta_c = 0;
				_delta.delta_r = -1;
				break;
			}
			//�� �����
			if(_delta.delta_r = -1)
			{
				_delta.delta_c = 1;
				_delta.delta_r = 0;
				break;
			}
			//����
			if(_delta.delta_c = 1)
			{
				_delta.delta_c = 0;
				_delta.delta_r = 1;
				break;
			}
			good_pos = Board_Position();
			_delta = delta();
		}
		while(false);
		
	}

	void Computer_Player::form_pos_to_set_ship(Position_Conteiner& pos_cont)
	{
		pos_cont.clear();
		for(int i = 1; i <= count_of_deck; i++)
		{
			for(int j = count_of_deck - i + 1; j > 0; j--)
			{
				Position_Conteiner new_pos;
				form_pos_ship(i - 1, new_pos);
				pos_cont.insert(pos_cont.end(), new_pos[0]);
				pos_cont.insert(pos_cont.end(), new_pos[1]);
			}
		}
	}
	void Computer_Player::form_pos_ship(int deck, Position_Conteiner& out)
	{
		out.clear();
		Board_Position first = rand_pos();
		int r, c;
		//r ��� � ���� 1 ����� 0
		r = rand()%2;
		c = 1 - r;
		r *= deck;
		c *= deck;
		Board_Position second(first.get_row() + r, first.get_column() + c); 
		out.push_back(first);
		out.push_back(second);
	}

}