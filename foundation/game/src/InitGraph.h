#ifndef FIGHT_INITGRAPH_H
#define FIGHT_INITGRAPH_H

#include"stdafx.h"
#include"Canvas2D.h"
#include"Window_Func.h"
namespace fight
{
	class InitGraph
	{
	public:
		InitGraph(Canvas2D canvas):_canvas2D(canvas)
		{}
		~InitGraph()
		{}

		void graph_main(int argc, char** argv, int w, int h);

	private:
		Canvas2D _canvas2D;
		void init_func();
	};
}

#endif