#include"stdafx.h"
#include"Texture.h"
namespace fight
{
#define count 3
	//Texture_Board::Texture_Board(int width, int height):_width(width), _height(height)
	//{
	//	/*image = new unsigned char ** [_height];
	//	for(int i = 0; i < _height; i++)
	//	{
	//		image[i] = new unsigned char*[_width];
	//		for(int j = 0; j < count; j++)
	//		{
	//			image[i][j] = new unsigned char;
	//		}
	//	}*/
	//}
	Texture_Board::~Texture_Board()
	{
		
		/*for (int i = 0; i < _height; i++)
		{
			for(int j = 0; j < _width; j++)
			{
				delete image[i][j];
			}
			delete []image[i];
		}
		delete []image;*/
	}

	void Texture_Board::get_image(unsigned char image[texture_height][texture_width][3]) const
	{
		for(int i = 0; i < _height; i++)
		{
			for(int j = 0; j < _width; j++)
			{
				for(int k = 0; k < count; k++)
				{
					image[i][j][k] = _image[i][j][k];
				}
			}
		}
	}

	void Texture_Board::Create_Texture(color first_color, color second_color)
	{
		int res;
		for (int i = 0; i < _height; i++)
		{
			for (int j = 0; j < _width; j++)
			{
				res = ( ( (j & width_of_quad) == 0 ) ^ ( (i & width_of_quad) == 0 ) );
				if(res)
				{
					_image[i][j][0] = first_color.red;
					_image[i][j][1] = first_color.green;
					_image[i][j][2] = first_color.blue;
				}
				else
				{
					_image[i][j][0] = second_color.red;
					_image[i][j][1] = second_color.green;
					_image[i][j][2] = second_color.blue;
				}
			}
		}
	}
	void Texture_Board::Set_Color(int x_pos, int y_pos, color c)
	{
		if( ((x_pos * width_of_quad) > _width) || ((y_pos * width_of_quad) > _height)) return;
		
		for(int k = 0; k < 4; k++ )
		{
			for(int k1 = 0; k1 < 4; k1++ )
			{
				_image[y_pos * width_of_quad + k][x_pos * width_of_quad + k1][0] = c.red;
				_image[y_pos * width_of_quad + k][x_pos * width_of_quad + k1][1] = c.green;
				_image[y_pos * width_of_quad + k][x_pos * width_of_quad + k1][2] = c.blue;
			}
		}
	}
}