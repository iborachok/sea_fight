#include"stdafx.h"
#include"Window_Func.h"
#include"Window.h"
#include<Windows.h>
namespace fight
{
	void Window_Func::display_func()
	{
		glClearColor(1,1,1,0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_window.draw_scen();
		glutSwapBuffers();
	}
	void Window_Func::reshape_func(int w, int h)
	{
		glViewport(0, 0, w, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0, w, 0, h);
		glMatrixMode(GL_MODELVIEW);
	    glLoadIdentity();
		_window.set_width(w);
		_window.set_height(h);
		//_window.get_colection().init_figures(w, h);
	}
	void Window_Func::key_func(unsigned char key, int x , int y)
	{
		Position_Conteiner conteiner, conteiner_second;
		switch (key)
		{
		case 'a': 
			_window.get_colection().re_init_figures();
			break;
		case 13:
			_window.set_state(true);
			_window.get_colection().get_pos_conteiner_from_sheep(conteiner);
			_game.get_first_player()->form_pos_to_set_ship(conteiner);
			//_game.get_second_player()->form_pos_to_set_ship(conteiner_second);
			CreateThread(0, 0, (LPTHREAD_START_ROUTINE)func_thread, 0,0,0);
			break;
		default:
			break;
		}
		glutPostRedisplay();
	}

	void Window_Func::spec_key_func(int key, int x , int y)
	{
		
	}

	void Window_Func::mouse_motion_func(int x, int y)
	{
		do
		{
			if(_window.get_state())
			{
				break;
			}
			if(x1 == 0 && y1 == 0)
			{
				break;
			}
			Figure_Colection& colection = _window.get_colection();
			
			Figure& _current_figure = colection.in_figure(Position(x,_window.get_height() - y));
			if(_current_figure.is_valid())
			{
				_current_figure.move_to_delta(x - x1, -(y - y1)); 
			}
			glutPostRedisplay();
		}
		while(false);
		x1 = x;
		y1 = y;


	}
	void Window_Func::mouse_func(int button, int state, int x, int y)
	{
		if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
			x1 = 0;
			y1 = 0;
			Figure_Colection& colection = _window.get_colection();

			//��� ��������
			if(_window.get_state())
			{
				int x_pos, y_pos;
				Figure& board = *(colection.get_colection().end() - 1);
				colection.convert_prom_pos_koordinat_to_board(board, Position(x, _window.get_height() - y), x_pos, y_pos);
				_game.get_first_player()->set_pos_of_move(Board_Position(x_pos, y_pos));
			}
		}
		glutPostRedisplay();
	}

	void Window_Func::set_game(Game game)
	{
		_game = game;
	}

	void Window_Func::action_with_move(bool is_good_pos, Board_Position b_pos, int id)
	{
		color c(0, 0, 0);
		if(is_good_pos)
		{
			c.red = 255;
		}
		
		Figure& board = *(_window.get_colection().get_colection().end() - id);
		if(is_valid( b_pos))
		{
			board.get_texture().Set_Color(b_pos.get_row(), b_pos.get_column(), c);
			glutPostRedisplay();
		}
	}
	void Window_Func::action_with_move_1(bool is_good_pos, Board_Position b_pos)
	{
		action_with_move(is_good_pos, b_pos, 2);
	}
	void Window_Func::action_with_move_2(bool is_good_pos, Board_Position b_pos)
	{
		action_with_move(is_good_pos, b_pos, 1);
	}
	Window Window_Func::_window = Window(500, 500);
	Game Window_Func::_game = Game();
	int Window_Func::x1 = 0;
	int Window_Func::y1 = 0;
	void Window_Func::func_thread()
	{
		_game.Start_Game();
	}
}