#include"stdafx.h"
#include"Canvas2D.h"
#include"InitGraph.h"

using namespace fight;
int main(int argc, char** argv)
{
	Canvas2D canvas;
	InitGraph init_graph(canvas);
	init_graph.graph_main(argc, argv, 500, 500);
	return 0;
}