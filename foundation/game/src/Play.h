#ifndef FIGHT_PLAY_H
#define FIGHT_PLAY_H

#include"stdafx.h"
#include"Board_Model.h"

namespace fight
{
	#define max_len_of_cmd 150

	//����, ���� ������ ���� ���, �������� �����, ������ ���������� ��� ���� �������
	class Play
	{
	public:
		Play(Board_Model board):_board(board), _count_of_ship(0)
		{}

		~Play()
		{}

	public: 

		const Board_Model& get_board() const
		{
			return _board;
		}

		//������ ���
		bool move_attack(const Board_Position&);

		//�-��� �������� ������� �� ������ ���
		unsigned get_count_of_ship() const
		{
			return _count_of_ship;
		}
		
		//���� ������� ������� � �������� ����
		char* get_result_of_command() const
		{
			return (char*)cmd_result;
		}

		//�� ������� ������� ���
		bool is_the_end_of_move() const;

		//������ ������ �� �����
		bool set_ship(const Position_Conteiner&);

	private:
		char cmd_result[max_len_of_cmd]; 
		Board_Model _board;
		unsigned _count_of_ship;

	private:
		//���� �-��� �������� ������� �� ������ ���
		void set_count_of_ship(unsigned count)
		{
			_count_of_ship = count;
		}

		//���� ���� ������� ������� �� ����� ������� ���� �����
		void set_cmd_state();
		//���� ���� ������� ������� ����� �����������
		void set_cmd_state(char* cmd_state)
		{
			strcpy(cmd_result, cmd_state);
		}
	};
}

#endif