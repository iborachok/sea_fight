#include"stdafx.h"
#include"Canvas2D.h"

namespace fight
{
	void Canvas2D::Draw_Rectungular(Position rect[count_of_point])
	{
		glBegin(GL_QUADS);
		for (int i = 0; i < count_of_point; i++)
		{
			glVertex2i(rect[i].get_x(), rect[i].get_y());
		}
		glEnd();
	}
	
	void Canvas2D::Draw_Rectungular_with_Texture(Position rect[count_of_point], Texture_Board texture)
	{
		init_texture(texture);

		float number = count_of_line_in_texture / float(texture_height / (float)width_of_quad_dec);
		glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(rect[0].get_x(), rect[0].get_y());
		glTexCoord2f(0.0, number); glVertex2f(rect[1].get_x(), rect[1].get_y());
		glTexCoord2f(number, number); glVertex2f(rect[2].get_x(), rect[2].get_y());
		glTexCoord2f(number, 0.0); glVertex2f(rect[3].get_x(), rect[3].get_y());
		glEnd();
	}

	void Canvas2D::init()
	{
		glClearColor (1.0, 1.0, 1.0, 1.0);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
	}
	
	void Canvas2D::canvas_main(int argc, char**argv, int width, int height)
	{
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
		glutInitWindowSize(width, height);
		glutCreateWindow("sea_fight");
		init();
		glutReshapeFunc (_reshape_func);
		glutDisplayFunc(_display_func);
		glutSpecialFunc(_spec_key_func);
		glutKeyboardFunc(_key_func);
		glutMouseFunc(_mouse_func);
		glutMotionFunc(_mouse_motion_func);
		glutMainLoop();
	}
	void Canvas2D::init_texture(Texture_Board texture)
	{
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		unsigned char image[texture_height][texture_width][3];
		texture.get_image(image);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, texture_width, texture_height, 0, GL_RGB, GL_UNSIGNED_BYTE, &image[0][0][0]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
		glEnable(GL_TEXTURE_2D);
	}
}