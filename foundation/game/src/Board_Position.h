#ifndef FIGHT_BOARD_POSITION
#define FIGHT_BOARD_POSITION

#include"stdafx.h"

namespace fight
{
	class Board_Position
	{
	public:

		static const unsigned invalid_position = UINT_MAX;

		Board_Position();
		Board_Position(unsigned row,unsigned column): _row(row), _column(column)
		{}
		~Board_Position(){}

		bool operator<(const Board_Position& other) const;
		bool operator>(const Board_Position& other) const;
		bool operator==(const Board_Position& other) const;
		bool operator!=(const Board_Position& other) const;

		void set_row(unsigned row)
		{
			_row = row;
		}
		void set_column(unsigned column)
		{
			_column = column;
		}

		unsigned get_row() const
		{
			return _row;
		}
		unsigned get_column() const
		{
			return _column;
		}


	private:
		unsigned _row,
				 _column;
	};

	inline Board_Position::Board_Position(): _row(invalid_position), _column(invalid_position)
	{}

	inline bool Board_Position::operator<(const Board_Position& other) const
	{
		//return _row < other.get_row() &&  _column < other.get_column();
		return _row < other.get_row() || (_row == other.get_row() && _column < other.get_column());
	}

	inline bool Board_Position::operator>(const Board_Position& other) const
	{
		return other < *this;
	}


	inline bool Board_Position::operator==(const Board_Position& other) const
	{
		return *this<other && other<*this;
	}

	inline bool Board_Position::operator!=(const Board_Position& other) const
	{
		return *this!=other;
	}

	inline bool is_valid(const Board_Position& pos)
	{
		return pos.get_column()!=Board_Position::invalid_position && pos.get_row()!=Board_Position::invalid_position;
	}
}

#endif