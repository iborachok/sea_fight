#ifndef FIGHT_WINDOW_POSITION_H
#define FIGHT_WINDOW_POSITION_H

namespace fight 
{
	#define count_of_point 4

    class Position
    {
    public:
        Position()
		{}
		Position(int x, int y):_x(x), _y(y)
		{}
        ~Position()
		{}

        int get_x() const;
        int get_y() const;

		void set_x(int x)
		{
			_x = x;
		}
		void set_y(int y)
		{
			_y = y; 
		}

		bool operator<(const Position& other)const
		{
			return _x < other.get_x() && _y < other.get_y();
		}
		bool operator>(const Position& other)const
		{
			return other < *this;
		}
		bool operator==(const Position& other)const
		{
			return *this < other && other > *this;
		}

    private:
        int _x;
        int _y;
    };

    inline int Position::get_x() const
    {
        return _x;
    }

    inline int Position::get_y() const
    {
        return _y;
    }


}

#endif 


