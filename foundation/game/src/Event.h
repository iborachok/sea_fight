#ifndef FIGHT_EVENT_H
#define FIGHT_EVENT_H

#include"WindowPosition.h"

namespace fight
{
	//����, ���� � ������������� ������ ��䳿
	class Event
	{
	public:
		enum Type
		{
			Event_Type_NONE = 0,
			Event_Type_CLOSE = 1,
			Event_Type_KEYBOARD = 2,
			Event_Type_MOUSE = 3
		};
		
		Event(Type type):_type(type)
		{}
		virtual ~Event()
		{}

		virtual Type get_Type() const
		{
			return _type;
		}

	private:
		Type _type;
	};

	class KeyBoard_Event: public Event
	{
	public:
		enum Kind
		{
			Kind_None,
			Kind_Pressed,
			Kind_Released
		};
		enum Key
		{
			Key_None,
			Key_Eec,
			Key_Enter,
			Key_Space
		};

	public:
		KeyBoard_Event(Kind kind, Key key):Event(Event::Event_Type_KEYBOARD), _kind(kind), _key(key)
		{}
		KeyBoard_Event():Event(Event::Event_Type_KEYBOARD)
		{}
		virtual ~KeyBoard_Event()
		{}
		Kind get_Kind() const
		{
			return _kind;
		}
		Key get_Key() const
		{
			return _key;
		}
	private:
		Kind _kind;
		Key _key;
	};

	class MouseEvent: public Event
	{
	public:
		enum Kind
		{
			Kind_None,
			Kind_Pressed,
			Kind_Released
		};
		enum Button
		{
			Button_None,
			Button_Left,
			Button_Right,
			Button_Middle
		};
	public:
		MouseEvent():Event(Event::Event_Type_MOUSE)
		{}
		MouseEvent(Button button, Position position):Event(Event::Event_Type_MOUSE), _button(button), _position(position)
		{}
		virtual ~MouseEvent()
		{}

		const Position& get_cursor_position() const
		{
			return _position;
		}
		Button get_button() const
		{
			return _button;
		}

	private:
		Button _button;
		Position _position;
	};

}

#endif