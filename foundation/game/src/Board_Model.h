#ifndef FIGHT_BOARD_MODEL_H
#define FIGHT_BOARD_MODEL_H

#include"stdafx.h"
#include"Board_Position.h"

namespace fight
{
	//���� ���� �������� ���������� ��� ���� ������� ����
	class Cell
	{
	public:
		Cell():_cellflag(CellFlags_Empty)
		{}
			
		bool is_empty() const 
		{
			return (_cellflag == CellFlags_Empty); 
		}
		bool is_figure() const {return (_cellflag & CellFlags_Fig)!=0; }
		bool is_selected() const {return (_cellflag & CellFlags_Selected)!=0; }
		bool is_selected_figure() const {return is_selected() && is_figure(); }
		bool is_end_figure_left() const {return (_cellflag & CellFlags_End_Fig_Left)!=0; }
		bool is_end_figure_right() const {return (_cellflag & CellFlags_End_Fig_Right)!=0; }
		//bool is_end_of_selected_figure() const {return is_selected_figure() && is_end_figure(); }

		void set_figure()
		{
			_cellflag |= CellFlags_Fig;
		}

		void set_selected()
		{
			_cellflag |= CellFlags_Selected;
		}

		void set_end_fig_left()
		{
			_cellflag |= CellFlags_End_Fig_Left;
		}
		void set_end_fig_right()
		{
			_cellflag |= CellFlags_End_Fig_Right;
		}
		void set_empty()
		{
			_cellflag = CellFlags_Empty;
		}

		~Cell()
		{}

	private:
		enum CellFlags
		{
			CellFlags_Empty			= 0x01,
			CellFlags_Selected		= 0x02,
			CellFlags_Fig			= 0x04,
			CellFlags_End_Fig_Left	= 0x08,
			CellFlags_End_Fig_Right = 0x0A
		};
		
	private:
		unsigned int _cellflag;
	};
	
	typedef std::vector<Board_Position> Position_Conteiner;
	typedef Position_Conteiner::const_iterator pos_cont_iterator;

	//����, ���� ������ ���������� ��� �����
	class Board_Model
	{
	public:
		class  Move
		{
		public:
			typedef std::vector<Move> Move_Container;

			Move()
			 {}
			Move(Cell cell): _cell(cell), _state(empty)
			 {}
			
			~ Move()
			{}

			//���� ������� ��� ����� �������
			enum move_state
			{
				attack = 1,
				is_selected_dest = 2,
				empty = 3
			};

			bool is_attack() const
			{
				return _state == attack;
			}
			bool is_selecteddest() const
			{
				return _state == is_selected_dest;
			}
			bool move_attac(const Board_Position& dest);

			void set_cell(const Cell& cell)
			{
				_cell = cell;
			}
		
		private:
			Cell _cell;
			move_state _state;
		private:
			void set_state(const move_state& state)
			{
				_state = state;
			}
			
			//�� ������� ��� �� �������
			bool is_possible_attack(const Board_Position& dest);		
		};

	public:
		//Board_Model();
		Board_Model(int count_column, int count_row):_state(no_valid_dest), count_column(count_column), count_row(count_row)
		{
			init_board(count_column, count_row);
		}
		~Board_Model()
		{}

		//typedef std::vector<Board_Position> Position_Conteiner;

		//���� ������� �������
		enum State
		{
			empty = 1,			// ������
			kill = 2,			// �������� �������
			not_kill = 3,		// �������� ��������
			bed_attack = 4,		// ��� �� ����� ��������
			no_valid_dest = 5	// ������� ���� ������ �����
		};

		int get_state() const
		{
			return _state;
		}

		//������ ������� �����
		void set_boundary(const Board_Position& first, const Board_Position& second);

		//�� ������� � ����� �����
		bool isvalid(const Board_Position& dest) const;
		
		//�� ����� ������� ���
		bool is_valid_pos(const Board_Position& dest);

		//���� ��������: ����� ������� �� �������
		bool set_ship(const Board_Position& f_pos, const Board_Position& l_pos);

		//������� ���
		bool move_attac(const Board_Position& dest);

		//������ ����
		void get_possible_move(Position_Conteiner& out) const;

		//��������� ����� � ���������� ����
		void reset()
		{
			init_board(count_column, count_row);
		}

		//�� ������� ������: ������ � ��������

		bool is_good_move() const
		{
			return _state == kill || _state == not_kill;
		}

		/*bool is_valid_move() const
		{
			return is_good_move() || _state == empty;
		}*/

		//������� ���� ������� �������
		bool IsKill() const
		{
			return _state == kill;
		}
		bool IsNot_Kill() const
		{
			return _state == not_kill;
		}

	private:

		typedef std::map<Board_Position, Cell> Type_Cell;
		typedef std::map<Board_Position, Cell>::iterator Type_Cell_iterator;
		
		
	private:
		//����a � ������� ���������� �����
		Board_Position f_boundary, s_boundary;
		
		Type_Cell _board_cell;
		State _state;
		Move _move;
		int count_column, count_row;
	private:

		void init_board(int c_size, int r_size);

		void set_state(const State& state)
		{
			_state = state;
		}

		bool is_kill(const Board_Position& dest);
		bool isnotkill(const Board_Position& dest);

		std::pair<Board_Position&,Board_Position&> get_boundary_of_fig_in_this_line(const Board_Position& dest, const Board_Position& first, const Board_Position& second, bool is_row);

	};
	Position_Conteiner	get_array_of_position(const Board_Position& f_pos, const Board_Position& l_pos);
}


#endif