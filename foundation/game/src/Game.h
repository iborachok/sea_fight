#ifndef FIGHT_GAME_H
#define FIGHT_GAME_H
	
#include"Player.h"
namespace fight
{
	class Game
	{
	public:
		enum State
		{
			Game_Initcialized = 1,
			First_Player_Go = 2,
			Second_Player_Go = 3,
			Game_Finished = 4
		};
	public:
		Game()
		{}
		Game(Player* first_palyer, Player* secod_player): _first_player(first_palyer), _second_player(secod_player)
		{
			_state = Game_Initcialized;
			InitGame();
		}
		~Game()
		{}

		State get_state() const
		{
			return _state;
		}

		void Start_Game();

		Player* get_first_player()
		{
			return _first_player;
		}
		Player* get_second_player()
		{
			return _second_player;
		}

	private:
		Player *_first_player, *_second_player;
		State _state;

		//��������� ���
		void InitGame();

		//��������� �������, ���������� ������
		void InitPlayer(Player* player);

		//������ ���� ��� ������ who �� ������ where
		void Move(Player* who, Player* where, int id);
		
	};
}
#endif