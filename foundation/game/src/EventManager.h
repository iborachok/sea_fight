#ifndef FIGHT_EVENT_MANAGER
#define FIGHT_EVENT_MANAGER

#include"Event.h"
namespace fight
{
	typedef void (*EventChangeHandler)(Event* e);
	
	//����, ���� ����������� ������� ��䳿
	class EventManager
	{
	public:
	public:
		EventManager(){}
		~EventManager(){}

		void set_event_handler(EventChangeHandler handler)
		{
			_event_handler = handler;
		}

		void UpdateEvent(Event::Type type)
		{
			Event* p_event;
			switch (type)
			{
			case fight::Event::Event_Type_NONE: p_event = new Event(Event::Event_Type_NONE);
				break;
			case fight::Event::Event_Type_CLOSE:
				break;
			case fight::Event::Event_Type_KEYBOARD: p_event = new KeyBoard_Event();
				break;
			case fight::Event::Event_Type_MOUSE: p_event = new MouseEvent();
				break;
			}
			if(p_event != 0)
			{
				_event_handler(p_event);
			}
			delete p_event;
		}

	private:
		EventChangeHandler _event_handler;
	};
}
#endif