#ifndef FIGHT_PLAYER_H
#define FIGHT_PLAYER_H

#include"stdafx.h"
#include"Play.h"
#include"Board_Position.h"

namespace fight
{
	class Player
	{
	public:
		Player(Play play): _play(play){}
		virtual ~Player() {}
		virtual Board_Position& get_pos_of_move() = 0; 
		virtual void form_pos_to_set_ship(Position_Conteiner&) = 0;

		void (*action_with_move)(bool, Board_Position b_pos);
		
		virtual void move_attack(Board_Position& b_pos)
		{
			_play.move_attack(b_pos);
			if(_play.get_board().is_good_move())
			{
				action_with_move(true, b_pos);
			}
			else
			{
				action_with_move(false, b_pos);
			}
		}

		virtual Play& get_play()
		{
			return _play;
		}

		virtual bool get_state()
		{
			return _state;
		}

		void set_state(bool state)
		{
			_state = state;
		}

		virtual void set_play(const Play& p)
		{
			_play = p;
		}
		virtual void set_pos_of_move(const Board_Position& pos)
		{}
		virtual void reset_pos()
		{}
	private:
		Play _play;
		//�����, ��� ����������� �� ��������� ������� �������
		bool _state;
	};

	//����, ���� ����������� �� ��� ������
	class HumanPlayer: public Player
	{
	public:
		HumanPlayer(Play p) : Player(p)
		{
			is_selected = true;
		}
		virtual ~HumanPlayer()
		{}

		virtual Board_Position& get_pos_of_move()
		{
			while (!is_valid(current_pos))
				{
					Sleep(100);
				}
			return current_pos;
		}
		void set_pos_of_move(const Board_Position& pos)
		{
			//is_selected = false;
			current_pos = pos;
		}
		virtual void form_pos_to_set_ship(Position_Conteiner& pos_sheep)
		{
			get_play().set_ship(pos_sheep);
		}
		virtual void reset_pos()
		{
			current_pos = Board_Position();
		}
	private:
		Board_Position current_pos;
		bool is_selected;
	};



	//����, ���� ����������� �� ��� ���������
	class Computer_Player: public Player
	{
	public:
		Computer_Player(Play p) : Player(p)
		{}
		virtual ~Computer_Player()
		{}
		virtual Board_Position& get_pos_of_move();

		//������� ���
		//virtual void move_attack(Board_Position& b_pos);

		//���������� ������� ��� ��������� �������
		virtual void form_pos_to_set_ship(Position_Conteiner& out);

	private:
		Board_Position current_pos,		//������� �������
						good_pos;		//������� �����

		//���������, ��� �������� ������� � ����������� �� �������� ����� �����, �� ����
		struct delta
		{
			unsigned delta_r;
			unsigned delta_c;
			delta():delta_c(-1), delta_r(0)
			{}
		};

		delta _delta;

	private:
		//�������� ������ �������� �������
		Board_Position& rand_pos();

		//���������� ���� ������� �� ����� ����� ��� ��������� ����, ���������� � current_pos
		void init_good_pos_and_delta();

		//����� ������� ��� ������ �������
		void form_pos_ship(int deck, Position_Conteiner&);
	};

	

}

#endif